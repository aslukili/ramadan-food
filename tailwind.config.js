module.exports = {
  content: ["./public/**/*.{html,js}"],
  theme: {
    extend: {
      colors: {
        primary: '#BA1700',
        secondary: '#FF5F00'
      },
      fontFamily:{
        headers: ["Reem Kufi"]
      }
    },
  },
  plugins: [],
}
